﻿using System.Windows;

namespace GitForEV3
{
    /// <summary>
    /// Interaction logic for CommitConflictResolutionWindow.xaml
    /// </summary>
    public partial class CommitConflictResolutionWindow : Window
    {
        public bool KeepRepositoryChanges = false;

        public bool KeepProjectFileChanges = false;

        public CommitConflictResolutionWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Not Implemented");
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            KeepRepositoryChanges = true;
            Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            KeepProjectFileChanges = true;
            Close();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Not Implemented");
            return;
        }
    }
}
