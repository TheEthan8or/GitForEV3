﻿using System.Windows;
using System.Windows.Controls;

namespace GitForEV3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            // Initialize preferences and load previous session
            Preferences.Load();
            Session.Load();
            cbxProjectSelector.SelectedIndex = Session.SelectedProjectIndex;
            cbxProjectSelector.ItemsSource = Preferences.Projects;
            if (Preferences.Projects.Count != 0)
            {
                btnSettings.IsEnabled = true;
                btnSync.IsEnabled = true;
                cbxSync.IsEnabled = true;
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            ProjectConfigurationWindow projectConfigurationWindow = new ProjectConfigurationWindow();
            projectConfigurationWindow.SetupLayoutForAddingProject();
            projectConfigurationWindow.ShowDialog();
            // Refresh Project Selector
            cbxProjectSelector.ItemsSource = null;
            cbxProjectSelector.SelectedIndex = Session.SelectedProjectIndex;
            cbxProjectSelector.ItemsSource = Preferences.Projects;
            if (Preferences.Projects.Count != 0)
            {
                btnSettings.IsEnabled = true;
                btnSync.IsEnabled = true;
                cbxSync.IsEnabled = true;
            }
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            OptionsWindow optionsWindow = new OptionsWindow();
            optionsWindow.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Session.SelectedProjectIndex = cbxProjectSelector.SelectedIndex;
            Session.Save();
            ProjectActionWindow projectActionWindow = new ProjectActionWindow();
            projectActionWindow.SyncProject();
            projectActionWindow.ShowDialog();
        }

        private void cbxProjectSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxProjectSelector.ItemsSource == Preferences.Projects)
            {
                Session.SelectedProjectIndex = cbxProjectSelector.SelectedIndex;
                Session.Save();
            }
        }

        private void ComboBoxItem_Selected(object sender, RoutedEventArgs e)
        {
            Session.SelectedProjectIndex = cbxProjectSelector.SelectedIndex;
            Session.Save();
            ProjectActionWindow projectActionWindow = new ProjectActionWindow();
            projectActionWindow.CommitProject();
            projectActionWindow.ShowDialog();
        }

        private void cbxSync_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbxSync.SelectedIndex = -1;
        }

        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            Session.SelectedProjectIndex = cbxProjectSelector.SelectedIndex;
            Session.Save();
            ProjectConfigurationWindow projectConfigurationWindow = new ProjectConfigurationWindow();
            projectConfigurationWindow.SetupLayoutForEditingProject();
            projectConfigurationWindow.ShowDialog();
            // Refresh Project Selector
            Preferences.Load();
            cbxProjectSelector.ItemsSource = null;
            cbxProjectSelector.SelectedIndex = Session.SelectedProjectIndex;
            cbxProjectSelector.ItemsSource = Preferences.Projects;
            if (Preferences.Projects.Count == 0)
            {
                btnSettings.IsEnabled = false;
                btnSync.IsEnabled = false;
                cbxSync.IsEnabled = false;
            }
        }
    }
}
