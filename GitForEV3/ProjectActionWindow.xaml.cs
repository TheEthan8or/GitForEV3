﻿using LibGit2Sharp;
using System;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Windows;

namespace GitForEV3
{
    /// <summary>
    /// Interaction logic for ProjectActionWindow.xaml
    /// </summary>
    public partial class ProjectActionWindow : Window
    {
        BackgroundWorker bw = new BackgroundWorker();

        public ProjectActionWindow()
        {
            InitializeComponent();
        }

        public void SyncProject()
        {
            wdwProjectAction.Title = "Syncing project '" + Preferences.Projects[Session.SelectedProjectIndex].ProfileName + "'...";
            bw.WorkerReportsProgress = true;
            bw.DoWork += bw_SyncProject;
            bw.ProgressChanged += bw_ProgressChanged;
            bw.RunWorkerCompleted += bw_RunWorkerCompleted;
            bw.RunWorkerAsync();
        }

        public void CommitProject()
        {
            tblStatus.Text = "Committing project '" + Preferences.Projects[Session.SelectedProjectIndex].ProfileName + "'...";
            bw.DoWork += bw_CommitProject;
            bw.RunWorkerCompleted += bw_RunWorkerCompleted;
            bw.RunWorkerAsync();
        }

        private void bw_CommitProject(object sender, DoWorkEventArgs e)
        {
            try
            {
                new Repository(Preferences.Projects[Session.SelectedProjectIndex].RepositoryPath);
                if (File.Exists(Preferences.Projects[Session.SelectedProjectIndex].ZipFilePath))
                {
                    ZipFile.OpenRead(Preferences.Projects[Session.SelectedProjectIndex].ZipFilePath).Dispose();
                }
            }
            catch (RepositoryNotFoundException ex)
            {
                e.Result = ex;
                return;
            }
            catch (InvalidDataException ex)
            {
                e.Result = ex;
                return;
            }

            try
            {
                Session.CommitCurrentProject();
            }
            catch (Exception ex)
            {
                e.Result = ex;
                return;
            }
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Result == null)
            {
                MessageBox.Show("Operation successful!", "GitForEV3", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else if (e.Result is RepositoryNotFoundException)
            {
                MessageBox.Show("Repository path does not point to a valid repository. Your path may be incorrect, or the repository may be corrupt.", "Invalid repository", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (e.Result is InvalidDataException)
            {
                MessageBox.Show("Project file path does not point to a valid .ev3 file. Your path may be incorrect, or the project file may be corrupt.", "Invalid project file", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (e.Result is LibGit2SharpException && (e.Result as Exception).Message is "There is no tracking information for the current branch.")
            {
                MessageBox.Show("Syncing the repository requires it to have a remote. Specify a remote and try again.", "No remotes specified", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                MessageBox.Show("Unknown error: " + (e.Result as Exception).Message + "\r\n" + (e.Result as Exception).StackTrace + "\r\n\r\n" + (e.Result as Exception).InnerException, "GitForEV3", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            Close();
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            string[] results = (string[])e.UserState;
            tblStatus.Text = results[e.ProgressPercentage];
            pgbProgress.IsIndeterminate = false;
            pgbProgress.Value = e.ProgressPercentage;
        }

        private void bw_SyncProject(object sender, DoWorkEventArgs e)
        {
            string[] workerResult = new string[6];
            workerResult[0] = "Committing changes...";
            workerResult[1] = "Pulling changes to local repository and saving them to project...";
            workerResult[2] = "Pushing changes to remote repository...";
            workerResult[3] = "Operation successful!";

            try
            {
                new Repository(Preferences.Projects[Session.SelectedProjectIndex].RepositoryPath);
                if (File.Exists(Preferences.Projects[Session.SelectedProjectIndex].ZipFilePath))
                {
                    ZipFile.OpenRead(Preferences.Projects[Session.SelectedProjectIndex].ZipFilePath).Dispose();
                }
            }
            catch (RepositoryNotFoundException ex)
            {
                e.Result = ex;
                return;
            }
            catch (InvalidDataException ex)
            {
                e.Result = ex;
                return;
            }
            catch (Exception ex)
            {
                e.Result = ex;
                return;
            }

            // Commit changes and make contents of project file same as contents of repository
            bw.ReportProgress(0, workerResult);
            try
            {
                Session.CommitCurrentProject();
            }
            catch (Exception ex)
            {
                e.Result = ex;
                return;
            }

            // Pull git repository and save changes to project file
            bw.ReportProgress(1, workerResult);
            try
            {
                Session.PullCurrentProject(true);
            }
            catch (MergeFetchHeadNotFoundException ex)
            {
                e.Result = ex;
                return;
            }
            catch (LibGit2SharpException ex)
            {
                e.Result = ex;
                return;
            }
            catch (Exception ex)
            {
                e.Result = ex;
                return;
            }

            // Push repository
            bw.ReportProgress(2, workerResult);
            Session.PushCurrentProject(true);

            // Report success
            bw.ReportProgress(3, workerResult);
        }
    }
}
