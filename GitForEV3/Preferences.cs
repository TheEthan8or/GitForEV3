﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using ZipVersionControlLib;

namespace GitForEV3
{
    class Preferences
    {
        public static List<Profile> Projects { get; private set; } = new List<Profile>();
        public static string GitName { get; set; } = "";
        public static string GitEmail { get; set; } = "";

        public static void Load()
        {
            // Reset profiles list
            Projects.Clear();
            // Save file path to variable
            string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "GitForEV3\\preferences.xml");
            // Check if AppData directory exists
            if (!Directory.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "GitForEV3")))
            {
                Directory.CreateDirectory(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "GitForEV3"));
            }
            // Load xml preferences file

            if (File.Exists(filePath))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(filePath);
                // Add projects to projects list
                XmlNodeList projectNodes = doc.DocumentElement.SelectNodes("Projects/Project");
                foreach (XmlNode projectNode in projectNodes)
                {
                    string name = projectNode.Attributes["Name"].Value;
                    string projectFilePath = projectNode.Attributes["ProjectFilePath"].Value;
                    string repositoryPath = projectNode.Attributes["RepositoryPath"].Value;
                    string username = projectNode.Attributes["Username"].Value;
                    byte[] password = Convert.FromBase64String(projectNode.Attributes["EncryptedPassword"].Value);
                    DateTimeOffset latestCommitTime = DateTimeOffset.Parse(projectNode.Attributes["LatestCommitTime"].Value);
                    byte[] projectFileHash = Convert.FromBase64String(projectNode.Attributes["ProjectFileHash"].Value);
                    Projects.Add(new Profile() { ProfileName = name, ZipFilePath = projectFilePath, RepositoryPath = repositoryPath, Username = username, Password = password, LatestCommitTime = latestCommitTime, ZipFileHash = projectFileHash });
                }
                // Initialize settings
                GitName = doc.DocumentElement.SelectSingleNode("Options/GitName").InnerText;
                GitEmail = doc.DocumentElement.SelectSingleNode("Options/GitEmail").InnerText;
            }
        }

        public static void Save()
        {
            string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "GitForEV3\\preferences.xml");
            // Save profiles to file
            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null); // Create xml declaration
            doc.AppendChild(docNode);
            XmlNode gitForEV3Node = doc.CreateElement("GitForEV3"); // Create Git for EV3 node
            doc.AppendChild(gitForEV3Node);
            XmlNode projectsNode = doc.CreateElement("Projects"); // Create projects node
            gitForEV3Node.AppendChild(projectsNode);
            for (int i = 0; i <= Projects.Count() - 1; i++)
            {
                XmlNode projectNode = doc.CreateElement("Project"); // Create project node
                projectNode.Attributes.Append(doc.CreateAttribute("Name")).Value = Projects[i].ProfileName;
                projectNode.Attributes.Append(doc.CreateAttribute("ProjectFilePath")).Value = Projects[i].ZipFilePath;
                projectNode.Attributes.Append(doc.CreateAttribute("RepositoryPath")).Value = Projects[i].RepositoryPath;
                projectNode.Attributes.Append(doc.CreateAttribute("Username")).Value = Projects[i].Username;
                projectNode.Attributes.Append(doc.CreateAttribute("EncryptedPassword")).Value = Convert.ToBase64String(Projects[i].Password);
                projectNode.Attributes.Append(doc.CreateAttribute("LatestCommitTime")).Value = Projects[i].LatestCommitTime.ToString();
                projectNode.Attributes.Append(doc.CreateAttribute("ProjectFileHash")).Value = Convert.ToBase64String(Projects[i].ZipFileHash);
                projectsNode.AppendChild(projectNode);
            }
            XmlNode optionsNode = doc.CreateElement("Options"); // Create options node
            gitForEV3Node.AppendChild(optionsNode);
            XmlNode gitNameNode = doc.CreateElement("GitName"); // Create git name node
            optionsNode.AppendChild(gitNameNode);
            gitNameNode.InnerText = GitName;
            XmlNode gitEmailNode = doc.CreateElement("GitEmail"); // Create git email node
            optionsNode.AppendChild(gitEmailNode);
            gitEmailNode.InnerText = GitEmail;
            doc.Save(filePath);
        }
    }
}
