﻿using LibGit2Sharp;
using System;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;
using ZipVersionControlLib;

namespace GitForEV3
{
    /// <summary>
    /// Interaction logic for ProjectConfigurationWindow.xaml
    /// </summary>
    public partial class ProjectConfigurationWindow : Window
    {
        public ProjectConfigurationWindow()
        {
            InitializeComponent();
        }

        public void SetupLayoutForAddingProject()
        {
            wdwConfigureProject.Title = "Add project";
            btnRemoveProject.Visibility = Visibility.Hidden;
            CompleteInformationCheck();
        }

        public void SetupLayoutForEditingProject()
        {
            wdwConfigureProject.Title = "Project settings";
            txtDisplayName.Text = Preferences.Projects[Session.SelectedProjectIndex].ProfileName;
            txtProjectFile.Text = Preferences.Projects[Session.SelectedProjectIndex].ZipFilePath;
            txtGitRepository.Text = Preferences.Projects[Session.SelectedProjectIndex].RepositoryPath;
            Preferences.Projects.RemoveAt(Session.SelectedProjectIndex);
            CompleteInformationCheck();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // Select project file
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            if (txtProjectFile.Text != "")
            {
                dlg.InitialDirectory = System.IO.Path.GetDirectoryName(txtProjectFile.Text);
                dlg.FileName = System.IO.Path.GetFileName(txtProjectFile.Text);
            }
            dlg.DefaultExt = "ev3";
            dlg.OverwritePrompt = false;
            dlg.Title = "Select Project File";
            dlg.Filter = "EV3 Project File (*.ev3)|*.ev3";
            dlg.FilterIndex = 1;
            dlg.RestoreDirectory = true;
            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                string filename = dlg.FileName;
                txtProjectFile.Text = filename;
            }
            CompleteInformationCheck();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            // Select git repository
            using (var dialog = new FolderBrowserDialog())
            {
                if (txtGitRepository.Text != "")
                {
                    dialog.SelectedPath = txtGitRepository.Text;
                }
                DialogResult result = dialog.ShowDialog();
                if (dialog.SelectedPath != "")
                {
                    txtGitRepository.Text = dialog.SelectedPath;
                }
            }
            CompleteInformationCheck();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            // Save project settings
            Preferences.Projects.Add(new Profile() { ProfileName = txtDisplayName.Text, ZipFilePath = txtProjectFile.Text, RepositoryPath = txtGitRepository.Text, Username = "", Password = ProtectedData.Protect(Encoding.Unicode.GetBytes(""), null, DataProtectionScope.CurrentUser), ZipFileHash = Encoding.Unicode.GetBytes("") });
            Preferences.Projects.Sort(delegate (Profile p1, Profile p2) { return p1.ProfileName.CompareTo(p2.ProfileName); });
            Preferences.Save();
            Session.SelectedProjectIndex = Preferences.Projects.FindIndex(profile => profile.ProfileName.Equals(txtDisplayName.Text, StringComparison.Ordinal));
            Session.Save();
            Close();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void txtDisplayName_TextChanged(object sender, TextChangedEventArgs e)
        {
            CompleteInformationCheck();
        }

        private void CompleteInformationCheck()
        {
            btnDone.IsEnabled = true;
            txtDisplayName.Background = Brushes.White;
            txtProjectFile.Background = Brushes.White;
            txtGitRepository.Background = Brushes.White;
            if (txtDisplayName.Text != "")
            {
                if (Preferences.Projects.Find(profile => profile.ProfileName.Equals(txtDisplayName.Text, StringComparison.Ordinal)) != null)
                {
                    btnDone.IsEnabled = false;
                    txtDisplayName.Background = Brushes.Red;
                }
            }
            else
            {
                btnDone.IsEnabled = false;
            }
            if (txtProjectFile.Text != "")
            {
                try
                {
                    ZipFile.OpenRead(txtProjectFile.Text).Dispose();
                }
                catch (InvalidDataException)
                {
                    btnDone.IsEnabled = false;
                    txtProjectFile.Background = Brushes.Red;
                }
                catch (IOException) { }
            }
            else
            {
                btnDone.IsEnabled = false;
            }
            if (txtGitRepository.Text != "")
            {
                if (Directory.Exists(Path.Combine(txtGitRepository.Text, ".git")))
                {
                    try
                    {
                        //new Repository(txtGitRepository.Text);
                    }
                    catch (RepositoryNotFoundException)
                    {
                        btnDone.IsEnabled = false;
                        txtGitRepository.Background = Brushes.Red;
                    }
                }
                else
                {
                    btnDone.IsEnabled = false;
                    txtGitRepository.Background = Brushes.Red;
                }
            }
            else
            {
                btnDone.IsEnabled = false;
            }
        }

        private void btnRemoveProject_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = System.Windows.MessageBox.Show("Are you sure you want to remove this profile?", "Remove profile", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                Preferences.Save();
                if (Session.SelectedProjectIndex == Preferences.Projects.Count)
                {
                    Session.SelectedProjectIndex--;
                }
                Close();
            }
        }
    }
}
