﻿using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Xml;

namespace GitForEV3
{
    class Session
    {
        public static int SelectedProjectIndex { get; set; } = -1;

        public static void Load()
        {

            // Save file path to variable
            string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "GitForEV3\\session.xml");
            // Check if AppData directory exists
            if (!Directory.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "GitForEV3")))
            {
                Directory.CreateDirectory(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "GitForEV3"));
            }
            // Load xml session file
            if (File.Exists(filePath))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(filePath);
                // Load previous session info
                SelectedProjectIndex = Convert.ToInt32(doc.DocumentElement.SelectSingleNode("SelectedProjectIndex").InnerText);
            }
        }

        public static void Save()
        {
            string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "GitForEV3\\session.xml");
            // Save profiles to file
            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null); // Create xml declaration
            doc.AppendChild(docNode);
            XmlNode gitForEV3Node = doc.CreateElement("GitForEV3"); // Create GitForEV3 node
            doc.AppendChild(gitForEV3Node);
            XmlNode selectedProfileIndexNode = doc.CreateElement("SelectedProjectIndex"); // Create selected project index node
            gitForEV3Node.AppendChild(selectedProfileIndexNode);
            selectedProfileIndexNode.InnerText = SelectedProjectIndex.ToString();
            doc.Save(filePath);
        }

        private static List<String> DirSearch(string sDir)
        {
            List<String> files = new List<String>();
            foreach (string f in Directory.GetFiles(sDir))
            {
                files.Add(f);
            }
            foreach (string d in Directory.GetDirectories(sDir))
            {
                var temp = Path.GetFileName(d);
                if (sDir != Preferences.Projects[SelectedProjectIndex].RepositoryPath || (sDir == Preferences.Projects[SelectedProjectIndex].RepositoryPath && Path.GetFileName(d) != ".git"))
                {
                    files.AddRange(DirSearch(d));
                }
            }

            return files;
        }

        private static Signature GetSignature(DateTime signatureDate)
        {
            // Try to get name and email
            try
            {
                return new Signature(Preferences.GitName, Preferences.GitEmail, signatureDate);
            }
            catch (IndexOutOfRangeException)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    OptionsWindow optionsWindow = new OptionsWindow();
                    // Show user input dialog to set name and email
                    MessageBox.Show("Please enter a name and email in the next dialog.", "Name and email required", MessageBoxButton.OK, MessageBoxImage.Information);
                    optionsWindow.ShowDialog();
                });
                return GetSignature(signatureDate);
            }
            catch (ArgumentException)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    OptionsWindow optionsWindow = new OptionsWindow();
                    // Show user input dialog to set name and email
                    MessageBox.Show("Please enter a name and email in the next dialog.", "Name and email required", MessageBoxButton.OK, MessageBoxImage.Information);
                    optionsWindow.ShowDialog();
                });
                return GetSignature(signatureDate);
            }
        }

        public static void PullCurrentProject(bool firstAttempt)
        {
            Repository repo = new Repository(Preferences.Projects[SelectedProjectIndex].RepositoryPath);
            if (firstAttempt)
            {
                // Backup project file and delete unnecessary backups
                Directory.CreateDirectory(Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath) + ".bks"));
                CopyProjectFile(Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath) + ".bks", DateTime.UtcNow.ToString("yyyy-MM-dd HH-mm-ss") + Path.GetExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)));
                while (Directory.GetFiles(Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)) + ".bks").Length > 6)
                {
                    string[] files = Directory.GetFiles(Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)) + ".bks");
                    Array.Sort(files);
                    File.Delete(files[0]);
                }
            }
            // Try to pull repository
            try
            {
                Preferences.Projects[SelectedProjectIndex].Pull(new UsernamePasswordCredentials() { Username = Preferences.Projects[SelectedProjectIndex].Username, Password = Encoding.Unicode.GetString(ProtectedData.Unprotect(Preferences.Projects[SelectedProjectIndex].Password, null, DataProtectionScope.CurrentUser)) }, GetSignature(DateTime.Now));
                // Update latest commit time and ev3 file hash values in Profile
                if (repo.Head.Tip != null)
                {
                    Preferences.Projects[SelectedProjectIndex].LatestCommitTime = repo.Head.Tip.Committer.When;
                }
                else
                {
                    Preferences.Projects[SelectedProjectIndex].LatestCommitTime = DateTime.MinValue;
                }
                Preferences.Projects[SelectedProjectIndex].ZipFileHash = HashProjectFile();
                Preferences.Save();
            }
            catch (LibGit2SharpException ex)
            {
                if (ex.Message == "too many redirects or authentication replays" || ex.Message == "failed to set credentials: The parameter is incorrect.\r\n")
                {
                    if (firstAttempt)
                    {
                        Application.Current.Dispatcher.Invoke(() => { MessageBox.Show("Please type in your git credentials.", "Git credentials", MessageBoxButton.OK, MessageBoxImage.Information); });
                    }
                    else
                    {
                        Application.Current.Dispatcher.Invoke(() => MessageBox.Show("Incorrect username or password. Please try again.", "Username or password incorrect", MessageBoxButton.OK, MessageBoxImage.Information));
                    }
                    Application.Current.Dispatcher.Invoke(() => 
                    {
                        GitCredentialsWindow gitCredentialsWindow = new GitCredentialsWindow();
                        gitCredentialsWindow.ShowDialog();
                    });
                }
                else if (ex.Message == "There is no tracking information for the current branch.")
                {
                    throw;
                }
                else
                {
                    Application.Current.Dispatcher.Invoke(() => MessageBox.Show(ex.Message, "Pull error", MessageBoxButton.OK, MessageBoxImage.Error));
                }
                PullCurrentProject(false);
            }
            catch (IOException)
            {
                Application.Current.Dispatcher.Invoke(() => MessageBox.Show("Project file could not be accessed. Please make sure no other processes are using the file and that you have read/write access to it.", "Project file could not be read/written", MessageBoxButton.OK, MessageBoxImage.Error));
                PullCurrentProject(false);
            }
        }

        private static void CopyProjectFile(string destination)
        {
            try
            {
                File.Copy(Preferences.Projects[SelectedProjectIndex].ZipFilePath, destination);
            }
            catch (IOException)
            {
                Application.Current.Dispatcher.Invoke(() => MessageBox.Show("Project file could not be accessed. Please make sure no other processes are using the file and that you have read/write access to it.", "Project file could not be read/written", MessageBoxButton.OK, MessageBoxImage.Error));
                CopyProjectFile(destination);
            }
        }

        public static void PushCurrentProject(bool firstAttempt)
        {
            Repository repo = new Repository(Preferences.Projects[SelectedProjectIndex].RepositoryPath);
            // Try to pull repository
            try
            {
                Preferences.Projects[SelectedProjectIndex].Push(new UsernamePasswordCredentials() { Username = Preferences.Projects[SelectedProjectIndex].Username, Password = Encoding.Unicode.GetString(ProtectedData.Unprotect(Preferences.Projects[SelectedProjectIndex].Password, null, DataProtectionScope.CurrentUser)) });
            }
            catch (LibGit2SharpException ex)
            {
                if (ex.Message == "too many redirects or authentication replays")
                {
                    if (firstAttempt)
                    {
                        Application.Current.Dispatcher.Invoke(() => { MessageBox.Show("Please type in your git credentials.", "Git credentials", MessageBoxButton.OK, MessageBoxImage.Warning); });
                    }
                    else
                    {
                        Application.Current.Dispatcher.Invoke(() => MessageBox.Show("Incorrect username or password. Please try again.", "Username or password incorrect", MessageBoxButton.OK, MessageBoxImage.Information));
                    }
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        GitCredentialsWindow gitCredentialsWindow = new GitCredentialsWindow();
                        gitCredentialsWindow.ShowDialog();
                    });
                }
                else if (ex.Message == "cannot push non-fastforwardable reference")
                {
                    Application.Current.Dispatcher.Invoke(() => MessageBox.Show("The changes pulled from the remote have conflicted with the local changes.\r\nResolve these confilcts. Then, proceed by clicking 'OK'.", "Merge conflicts", MessageBoxButton.OK, MessageBoxImage.Information));
                    if (repo.RetrieveStatus().IsDirty)
                    {
                        // Commit changes
                        string commitMessage = "";
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            CommitMessageWindow commitMessageWindow = new CommitMessageWindow();
                            commitMessageWindow.btnNoCommit.IsEnabled = false;
                            commitMessageWindow.txtCommitMessage.Text = "Merge branch '" + repo.Head.FriendlyName + "' of " + repo.Network.Remotes.First().Url; ;
                            commitMessageWindow.ShowDialog();
                            commitMessage = commitMessageWindow.Message;
                        });
                        Commands.Stage(repo, "*");
                        DateTime signatureDate = DateTime.Now;
                        Commit commit = repo.Commit(commitMessage, GetSignature(signatureDate), GetSignature(signatureDate));
                    }
                    // Backup project file and delete unnecessary backups
                    Directory.CreateDirectory(Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath) + ".bks"));
                    File.Copy(Preferences.Projects[SelectedProjectIndex].ZipFilePath, Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath) + ".bks", DateTime.UtcNow.ToString("yyyy-MM-dd HH-mm-ss") + Path.GetExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)));
                    while (Directory.GetFiles(Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)) + ".bks").Length > 6)
                    {
                        string[] files = Directory.GetFiles(Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)) + ".bks");
                        Array.Sort(files);
                        File.Delete(files[0]);
                    }
                    // Replace project file
                    DeleteProjectFile();
                    using (ZipArchive zipArchive = ZipFile.Open(Preferences.Projects[SelectedProjectIndex].ZipFilePath, ZipArchiveMode.Create))
                    {
                        foreach (string f in DirSearch(Preferences.Projects[SelectedProjectIndex].RepositoryPath))
                        {
                            zipArchive.CreateEntryFromFile(f, f.Replace(Preferences.Projects[SelectedProjectIndex].RepositoryPath + @"\", ""));
                        }
                    }
                    // Update latest commit time and project file hash values in Profile
                    if (repo.Head.Tip != null)
                    {
                        Preferences.Projects[SelectedProjectIndex].LatestCommitTime = repo.Head.Tip.Committer.When;
                    }
                    else
                    {
                        Preferences.Projects[SelectedProjectIndex].LatestCommitTime = DateTime.MinValue;
                    }
                    Preferences.Projects[SelectedProjectIndex].ZipFileHash = HashProjectFile();
                    Preferences.Save();
                }
                else if (ex.Message == "There is no tracking information for the current branch.")
                {
                    throw;
                }
                else
                {
                    Application.Current.Dispatcher.Invoke(() => MessageBox.Show(ex.Message, "Push error", MessageBoxButton.OK, MessageBoxImage.Information));
                }
                PushCurrentProject(false);
            }
        }

        public static void CommitCurrentProject()
        {
            // Commit changes
            Repository repo = new Repository(Preferences.Projects[SelectedProjectIndex].RepositoryPath);
            bool repositoryChange = false;
            bool projectFileChange = false;
            byte[] repositoryHash;
            byte[] projectFileHash;
            // Check if there are uncommitted files in the repository
            if (repo.RetrieveStatus().IsDirty)
            {
                MessageBoxResult messageBoxResult = new MessageBoxResult();
                Application.Current.Dispatcher.Invoke(() => { messageBoxResult = MessageBox.Show("There are uncommitted changes in the repository. Would you like to include them in the commit? If you don't, they will be overwritten.\r\n(Note: Unresolved merge conflicts may be included in the commit, even if you click 'No'.", "Uncommitted changes", MessageBoxButton.YesNoCancel, MessageBoxImage.Question); });
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    repositoryChange = true;
                }
                else if (messageBoxResult == MessageBoxResult.No)
                {
                    repo.Reset(ResetMode.Mixed, repo.Head.Tip);
                }
                else if (messageBoxResult == MessageBoxResult.Cancel) { throw new NotImplementedException(); }
            }
            if (File.Exists(Preferences.Projects[SelectedProjectIndex].ZipFilePath))
            {
                // Check for differences between contents in repository and contents in project file
                // Delete Temp directory if exists
                if (Directory.Exists(Path.Combine(Path.GetTempPath(), "GitForEV3")))
                {
                    Directory.Delete(Path.Combine(Path.GetTempPath(), "GitForEV3"), true);
                }
                // Create Temp directory
                Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), "GitForEV3"));
                // Create temporary zip file of repository to hash
                using (ZipArchive zipArchive = ZipFile.Open(Path.Combine(Path.GetTempPath(), "GitForEV3\\" + Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath) + ".rep"), ZipArchiveMode.Create))
                {
                    foreach (string f in DirSearch(Preferences.Projects[SelectedProjectIndex].RepositoryPath))
                    {
                        zipArchive.CreateEntryFromFile(f, f.Replace(Preferences.Projects[SelectedProjectIndex].RepositoryPath + @"\", ""));
                    }
                }
                // Re-zip project file for correct hashing
                Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), "GitForEV3\\" + Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)));
                UnzipProjectFile(Path.Combine(Path.GetTempPath(), "GitForEV3\\" + Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)));
                using (ZipArchive zipArchive = ZipFile.Open(Path.Combine(Path.GetTempPath(), "GitForEV3\\" + Path.GetFileName(Preferences.Projects[SelectedProjectIndex].ZipFilePath)), ZipArchiveMode.Create))
                {
                    foreach (string f in DirSearch(Path.Combine(Path.GetTempPath(), "GitForEV3\\" + Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath))))
                    {
                        zipArchive.CreateEntryFromFile(f, f.Replace(Path.Combine(Path.GetTempPath(), "GitForEV3\\" + Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)) + @"\", "")); // Compress each file to zip file in Temp directory
                    }
                }
                // Hash files
                using (FileStream stream = File.OpenRead(Path.Combine(Path.GetTempPath(), "GitForEV3\\" + Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath) + ".rep")))
                {
                    repositoryHash = MD5.Create().ComputeHash(stream);
                    stream.Close();
                }
                using (FileStream stream = File.OpenRead(Path.Combine(Path.GetTempPath(), "GitForEV3\\" + Path.GetFileName(Preferences.Projects[SelectedProjectIndex].ZipFilePath))))
                {
                    projectFileHash = MD5.Create().ComputeHash(stream);
                    stream.Close();
                }
                // Delete temporary files
                Directory.Delete(Path.Combine(Path.GetTempPath(), "GitForEV3"), true);
                // Compare hashes
                if (!repositoryHash.SequenceEqual(projectFileHash))
                {
                    // Check for modifications to repository
                    Commit test = repo.Head.Tip;
                    if (repo.Head.Tip != null && (Preferences.Projects[SelectedProjectIndex].LatestCommitTime.Equals(DateTime.MinValue) || Preferences.Projects[SelectedProjectIndex].LatestCommitTime < repo.Head.Tip.Committer.When))
                    {
                        repositoryChange = true;
                    }

                    // Check for modifications to project file
                    if (!projectFileHash.SequenceEqual(Preferences.Projects[SelectedProjectIndex].ZipFileHash))
                    {
                        projectFileChange = true;
                    }
                }
                else
                {
                    repositoryChange = false;
                    projectFileChange = false;
                }

                // Run action depending on what has been changed
                if (repositoryChange && projectFileChange)
                {
                    bool keepRepositoryChanges = true;
                    bool keepProjectFileChanges = true;
                    Application.Current.Dispatcher.Invoke(() => 
                    {
                        CommitConflictResolutionWindow commitConflictResolutionWindow = new CommitConflictResolutionWindow();
                        if (repo.RetrieveStatus().IsDirty)
                        {
                            // Disable "Keep changes in project file" button
                            commitConflictResolutionWindow.btnProjectFileChanges.IsEnabled = false;
                        }
                        commitConflictResolutionWindow.ShowDialog();
                        keepRepositoryChanges = commitConflictResolutionWindow.KeepRepositoryChanges;
                        keepProjectFileChanges = commitConflictResolutionWindow.KeepProjectFileChanges;
                    });
                    if (keepRepositoryChanges && keepProjectFileChanges)
                    {
                        // Merge files
                        throw new NotImplementedException();
                        //Microsoft.VisualBasic.FileIO.FileSystem.CopyDirectory
                    }
                    else if (keepRepositoryChanges && !keepProjectFileChanges)
                    {
                        // Commit changes if there are uncommitted changes in the repository
                        if (repo.RetrieveStatus().IsDirty)
                        {
                            string commitMessage = "";
                            Application.Current.Dispatcher.Invoke(() => 
                            {
                                CommitMessageWindow commitMessageWindow = new CommitMessageWindow();
                                commitMessageWindow.btnNoCommit.IsEnabled = false;
                                commitMessageWindow.ShowDialog();
                                commitMessage = commitMessageWindow.Message;
                            });
                            Commands.Stage(repo, "*");
                            DateTime signatureDate = DateTime.Now;
                            Commit commit = repo.Commit(commitMessage, GetSignature(signatureDate), GetSignature(signatureDate));
                        }
                        // Backup project file and delete unnecessary backups
                        Directory.CreateDirectory(Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath) + ".bks"));
                        CopyProjectFile(Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath) + ".bks", DateTime.UtcNow.ToString("yyyy-MM-dd HH-mm-ss") + Path.GetExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)));
                        while (Directory.GetFiles(Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)) + ".bks").Length > 6)
                        {
                            string[] files = Directory.GetFiles(Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)) + ".bks");
                            Array.Sort(files);
                            File.Delete(files[0]);
                        }
                        // Replace project file
                        DeleteProjectFile();
                        using (ZipArchive zipArchive = ZipFile.Open(Preferences.Projects[SelectedProjectIndex].ZipFilePath, ZipArchiveMode.Create))
                        {
                            foreach (string f in DirSearch(Preferences.Projects[SelectedProjectIndex].RepositoryPath))
                            {
                                zipArchive.CreateEntryFromFile(f, f.Replace(Preferences.Projects[SelectedProjectIndex].RepositoryPath + @"\", ""));
                            }
                        }
                    }
                    else if (!keepRepositoryChanges && keepProjectFileChanges)
                    {
                        // Commit changes in project file and overwrite repository files
                        string commitMessage = "";
                        Application.Current.Dispatcher.Invoke(() => 
                        {
                            CommitMessageWindow commitMessageWindow = new CommitMessageWindow();
                            commitMessageWindow.btnNoCommit.IsEnabled = false;
                            commitMessageWindow.ShowDialog();
                            commitMessage = commitMessageWindow.Message;
                        });
                        Preferences.Projects[SelectedProjectIndex].Commit(commitMessage, GetSignature(DateTime.Now));
                    }
                }
                else if (repositoryChange && !projectFileChange && repo.RetrieveStatus().IsDirty)
                {
                    // Commit changes
                    string commitMessage = "";
                    Application.Current.Dispatcher.Invoke(() => 
                    {
                        CommitMessageWindow commitMessageWindow = new CommitMessageWindow();
                        commitMessageWindow.btnNoCommit.IsEnabled = false;
                        commitMessageWindow.ShowDialog();
                        commitMessage = commitMessageWindow.Message;
                    });
                    Commands.Stage(repo, "*");
                    DateTime signatureDate = DateTime.Now;
                    Commit commit = repo.Commit(commitMessage, GetSignature(signatureDate), GetSignature(signatureDate));
                    // Backup project file and delete unnecessary backups
                    Directory.CreateDirectory(Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath) + ".bks"));
                    File.Copy(Preferences.Projects[SelectedProjectIndex].ZipFilePath, Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath) + ".bks", DateTime.UtcNow.ToString("yyyy-MM-dd HH-mm-ss") + Path.GetExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)));
                    while (Directory.GetFiles(Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)) + ".bks").Length > 6)
                    {
                        string[] files = Directory.GetFiles(Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)) + ".bks");
                        Array.Sort(files);
                        File.Delete(files[0]);
                    }
                    // Replace project file
                    DeleteProjectFile();
                    using (ZipArchive zipArchive = ZipFile.Open(Preferences.Projects[SelectedProjectIndex].ZipFilePath, ZipArchiveMode.Create))
                    {
                        foreach (string f in DirSearch(Preferences.Projects[SelectedProjectIndex].RepositoryPath))
                        {
                            zipArchive.CreateEntryFromFile(f, f.Replace(Preferences.Projects[SelectedProjectIndex].RepositoryPath + @"\", ""));
                        }
                    }
                }
                else if (!repositoryChange && projectFileChange)
                {
                    // Commit changes in project file
                    string commitMessage = "";
                    Application.Current.Dispatcher.Invoke(() => 
                    {
                        CommitMessageWindow commitMessageWindow = new CommitMessageWindow();
                        commitMessageWindow.ShowDialog();
                        commitMessage = commitMessageWindow.Message;
                    });
                    if (commitMessage == "")
                    {
                        // Backup project file and delete unnecessary backups
                        Directory.CreateDirectory(Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath) + ".bks"));
                        File.Copy(Preferences.Projects[SelectedProjectIndex].ZipFilePath, Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath) + ".bks", DateTime.UtcNow.ToString("yyyy-MM-dd HH-mm-ss") + Path.GetExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)));
                        while (Directory.GetFiles(Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)) + ".bks").Length > 6)
                        {
                            string[] files = Directory.GetFiles(Path.Combine(Path.GetDirectoryName(Preferences.Projects[SelectedProjectIndex].ZipFilePath), Path.GetFileNameWithoutExtension(Preferences.Projects[SelectedProjectIndex].ZipFilePath)) + ".bks");
                            Array.Sort(files);
                            File.Delete(files[0]);
                        }
                        // Replace project file
                        DeleteProjectFile();
                        using (ZipArchive zipArchive = ZipFile.Open(Preferences.Projects[SelectedProjectIndex].ZipFilePath, ZipArchiveMode.Create))
                        {
                            foreach (string f in DirSearch(Preferences.Projects[SelectedProjectIndex].RepositoryPath))
                            {
                                zipArchive.CreateEntryFromFile(f, f.Replace(Preferences.Projects[SelectedProjectIndex].RepositoryPath + @"\", ""));
                            }
                        }
                    }
                    else
                    {
                        Preferences.Projects[SelectedProjectIndex].Commit(commitMessage, GetSignature(DateTime.Now));
                    }
                }
                else if (!repositoryChange && !projectFileChange && repo.RetrieveStatus().IsDirty)
                {
                    // Commit changes
                    string commitMessage = "";
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        CommitMessageWindow commitMessageWindow = new CommitMessageWindow();
                        commitMessageWindow.btnNoCommit.IsEnabled = false;
                        commitMessageWindow.ShowDialog();
                        commitMessage = commitMessageWindow.Message;
                    });
                    Commands.Stage(repo, "*");
                    DateTime signatureDate = DateTime.Now;
                    Commit commit = repo.Commit(commitMessage, GetSignature(signatureDate), GetSignature(signatureDate));
                }
            }
            else
            {
                // Commit any uncommitted changes in repository
                if (repo.RetrieveStatus().IsDirty)
                {
                    string commitMessage = "";
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        CommitMessageWindow commitMessageWindow = new CommitMessageWindow();
                        commitMessageWindow.btnNoCommit.IsEnabled = false;
                        commitMessageWindow.ShowDialog();
                        commitMessage = commitMessageWindow.Message;
                    });
                    Commands.Stage(repo, "*");
                    DateTime signatureDate = DateTime.Now;
                    Commit commit = repo.Commit(commitMessage, GetSignature(signatureDate), GetSignature(signatureDate));
                }
                // Create project file
                using (ZipArchive zipArchive = ZipFile.Open(Preferences.Projects[SelectedProjectIndex].ZipFilePath, ZipArchiveMode.Create))
                {
                    foreach (string f in DirSearch(Preferences.Projects[SelectedProjectIndex].RepositoryPath))
                    {
                        zipArchive.CreateEntryFromFile(f, f.Replace(Preferences.Projects[SelectedProjectIndex].RepositoryPath + @"\", ""));
                    }
                }
                // Hash project file
                projectFileHash = HashProjectFile();
            }
            // Update latest commit time and project file hash values in Profile
            if (repo.Head.Tip != null)
            {
                Preferences.Projects[SelectedProjectIndex].LatestCommitTime = repo.Head.Tip.Committer.When;
            }
            else
            {
                Preferences.Projects[SelectedProjectIndex].LatestCommitTime = DateTime.MinValue;
            }
            Preferences.Projects[SelectedProjectIndex].ZipFileHash = projectFileHash;
            Preferences.Save();
        }

        private static byte[] HashProjectFile()
        {
            try
            {
                using (FileStream stream = File.OpenRead(Preferences.Projects[SelectedProjectIndex].ZipFilePath))
                {
                    return MD5.Create().ComputeHash(stream);
                }
            }
            catch (IOException)
            {
                MessageBox.Show("Project file could not be accessed. Please make sure no other processes are using the file and that you have read/write access to it.", "Project file could not be read/written", MessageBoxButton.OK, MessageBoxImage.Error);
                return HashProjectFile();
            }
        }

        private static void DeleteProjectFile()
        {
            try
            {
                File.Delete(Preferences.Projects[SelectedProjectIndex].ZipFilePath);
            }
            catch (IOException)
            {
                MessageBox.Show("Project file could not be accessed. Please make sure no other processes are using the file and that you have read/write access to it.", "Project file could not be read/written", MessageBoxButton.OK, MessageBoxImage.Error);
                DeleteProjectFile();
            }
        }

        private static void UnzipProjectFile(string directory)
        {
            try
            {
                using (FileStream zipFile = new FileStream(Preferences.Projects[SelectedProjectIndex].ZipFilePath, FileMode.Open))
                {
                    new ZipArchive(zipFile, ZipArchiveMode.Update).ExtractToDirectory(directory);
                }
            }
            catch (IOException)
            {
                MessageBox.Show("Project file could not be accessed. Please make sure no other processes are using the file and that you have read/write access to it.", "Project file could not be read/written", MessageBoxButton.OK, MessageBoxImage.Error);
                UnzipProjectFile(directory);
            }
        }
    }
}
