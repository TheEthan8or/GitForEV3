﻿using System.Security.Cryptography;
using System.Text;
using System.Windows;

namespace GitForEV3
{
    /// <summary>
    /// Interaction logic for GitCredentialsWindow.xaml
    /// </summary>
    public partial class GitCredentialsWindow : Window
    {
        public GitCredentialsWindow()
        {
            InitializeComponent();
            txtUsername.Text = Preferences.Projects[Session.SelectedProjectIndex].Username;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // Encrypt password
            byte[] plainPassword = Encoding.Unicode.GetBytes(pwdPassword.Password);
            pwdPassword.Password = "";
            Preferences.Projects[Session.SelectedProjectIndex].Password = ProtectedData.Protect(plainPassword, null, DataProtectionScope.CurrentUser);
            Preferences.Projects[Session.SelectedProjectIndex].Username = txtUsername.Text;
            Preferences.Save();
            Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Not Implemented");
        }
    }
}
