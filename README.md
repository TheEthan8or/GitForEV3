# Git for EV3

Git for EV3 is a work-in-progress Windows application designed to version control EV3 Project files.

Since an EV3 file is actually a ZIP file full of XML files for each program, Git for EV3 uses [ZipVersionControl](https://github.com/TheEthan8or/ZipVersionControl) libraries to version control the contents of the Project file. When one person edits one program and another person edits another on a different computer, the changes will automatically combine with the click of a button.

Interested? Subscribe to this repository's notifications to get notified of updates in the future.

## Installation

See "Compiling from Source".

## Compiling from Source

Requirements:

* [Visual Studio 2019](https://visualstudio.microsoft.com/)

To compile from source, open `GitForEV3.sln` and build the project `GitForEV3`.

## Usage

Before using this program, create a git repository and clone it to your computer, or clone an existing repository. This repository will contain the unzipped contents of your EV3 file. You will also need to manage conflicts, so using a tool such as [Meld](meldmerge.org) can be very helpful.

In the main window, select File > Add project, and select your EV3 Project file and git repository you cloned. If your git repository already contains your EV3 programs, save the EV3 Project file as an empty file. Enter a display name, and click "Done".

To sync the program with both the local and remote repository, click "Sync". This will update both the remote repository, the local repository, and the EV3 Project.

## Important!

This project is currently **work-in-progress**. This means it is missing a few features and isn't totally user-friendly. Here are some things to be aware of:

- User-friendly conflict-management is not yet implemented. Instead, conflicts must be resolved by editing the XML files directly. Right now, you should only use this software if you know XML basics and how XML works in EV3 Project files.
- If conflicts are dealt with incorrectly, the Project file can be corrupted or programs might not appear. For example:
  - Committing unresolved changes
  - Deleting necessary data

If something does go wrong, Git for EV3 keeps six backups of the Project file in the folder `<ProjectFileName>.bks`. "Sync" the Project to update it, replace the current Project file with a backup, and re-sync. Still, it is recommended that you constantly **back up your project** in case something happens to it.

## Conflicts and Corruptions

If there is a conflict, **DO NOT CLICK "OK" UNTIL YOU HAVE RESOLVED IT**. To resolve it, use a utility such as [Meld](meldmerge.org), and resolve the conflicts in the git repository you cloned. Once you have, you may continue the operation.

Since the EV3 software keeps a list of each program in a single file, simply merging two program-creation commits can result in conflicts. Those are easy to resolve. However, when a conflict is caused by two commits modifying the same program, the only easy way to solve it is to keep one set of changes and undo the other.

To avoid messing things up:

* Always sync before and after making changes.

* Multiple people should never work on the same program at the same time.
* Never rename a MyBlock when somebody else is working on a program involving that MyBlock.
* Never create a program with the same name as another new program.

## Contributing
Feature requests are welcome. If you notice an issue, please add it to the issue tracker.

## License
See LICENSE.md.